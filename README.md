# Fahrenholz/Docker-Images

## What problem is solved

Gitlab pipelines run a long time if you want to add some internals to your container in the before_script-section. With this repository I want to externalize it in order to be able to make the CI faster for other projects.

## Is there any warranty?

No. But feel free to use it or to make some PRs.

## Which projects use it

- [fahrenholz/mayhem](https://www.gitlab.com/fahrenholz/mayhem)